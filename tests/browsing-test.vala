/*
 * Copyright (C) 2008 OpenedHand Ltd.
 * Copyright (C) 2008,2010 Zeeshan Ali (Khattak) <zeeshanak@gnome.org>.
 *
 * Author: Jussi Kukkonen <jku@openedhand.com>
 *         Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *
 * This file is part of gupnp-vala.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

using GLib;
using GUPnP;

/*
 * TODO:
 *  * call setlocale
 *  * SIGTERM handler?
 */

public class Test.BrowsingTest : Object {

    public static int main (string[] args) {
        Context ctxt;

        try {
            ctxt = new Context (null, null, 0);
        } catch (Error err) {
            critical (err.message);
            return 1;
        }

        /* We're interested in everything */
        ControlPoint cp = new ControlPoint (ctxt, "ssdp:all");
        cp.device_proxy_available.connect (on_device_proxy_available);
        cp.device_proxy_unavailable.connect (on_device_proxy_unavailable);
        cp.service_proxy_available.connect (on_service_proxy_available);
        cp.service_proxy_unavailable.connect (on_service_proxy_unavailable);
        cp.active = true;

        MainLoop loop = new MainLoop (null, false);
        loop.run();

        return 0;
    }

    private static void on_service_proxy_available (ControlPoint cp,
                                                    ServiceProxy proxy) {
        print ("Service available:\n");
        print ("\ttype: %s\n", proxy.service_type);
        print ("\tlocation: %s\n", proxy.location);
    }

    private static void on_service_proxy_unavailable (ControlPoint cp,
                                                      ServiceProxy proxy) {
        print ("Service unavailable:\n");
        print ("\ttype: %s\n", proxy.service_type);
        print ("\tlocation: %s\n", proxy.location);
    }

    private static void on_device_proxy_available (ControlPoint cp,
                                                   DeviceProxy  proxy) {
        print ("Device available:\n");
        print ("\ttype: %s\n", proxy.device_type);
        print ("\tlocation: %s\n", proxy.location);
    }

    private static void on_device_proxy_unavailable (ControlPoint cp,
                                                     DeviceProxy  proxy) {
        print ("Device unavailable:\n");
        print ("\ttype: %s\n", proxy.device_type);
        print ("\tlocation: %s\n", proxy.location);
    }
}
