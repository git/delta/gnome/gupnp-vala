/* gupnp-vala -- Vala bindings for GUPnP
 * Copyright (C) 2008 OpenedHand Ltd.
 *
 * Author: Jussi Kukkonen <jku@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

namespace GUPnP {
        public static void value_list_free (GLib.List<weak GLib.Value*> list) {
            foreach (var v in list) {
                v->unset ();
                GLib.Slice.free (sizeof (GLib.Value), v);
            }
        }

        public static void value_hash_free (GLib.HashTable<string, weak GLib.Value*> hash) {
            hash.for_each ((k, v) => {
                v->unset ();
                GLib.Slice.free (sizeof (GLib.Value), v);
            });
        }

        public class ServiceProxy {
                [CCode (has_construct_function = false)]
                public ServiceProxy ();
                
                public virtual signal void subscription_lost (GLib.Error reason);

                public bool end_action_hash (GUPnP.ServiceProxyAction action, [CCode (pos=-0.9)] GLib.HashTable<string, weak GLib.Value*> hash) throws GLib.Error;
                public bool end_action_list (GUPnP.ServiceProxyAction action, [CCode (pos=-0.9)] GLib.List<string> out_names, [CCode (pos=-0.8)] GLib.List<GLib.Type?> out_types, [CCode (pos=-0.7)] out GLib.List<weak GLib.Value*> out_values) throws GLib.Error;

                public bool send_action_hash (string action, [CCode (pos=-0.9)] GLib.HashTable<string, GLib.Value?> in_hash, [CCode (pos=-0.8)] GLib.HashTable<string, weak GLib.Value*> out_hash) throws GLib.Error;
                public bool send_action_list (string action, [CCode (pos=-0.9)] GLib.List<string> in_names, [CCode (pos=-0.8)] GLib.List<weak GLib.Value?> in_values, [CCode (pos=-0.7)] GLib.List<string> out_names, [CCode (pos=-0.6)] GLib.List<GLib.Type?> out_types, [CCode (pos=-0.5)] out GLib.List<weak GLib.Value*> out_values) throws GLib.Error;
        }
        
        public class Service {
                [CCode (has_construct_function = false)]
                public Service ();
                
                public virtual signal void notify_failed (GLib.List<Soup.URI> callback_urls, GLib.Error reason);
                public virtual signal void query_variable (string variable, ref GLib.Value value);
        }
        
        public class DeviceInfo {
                [CCode (has_construct_function = false)]
                public DeviceInfo ();
                
                public Soup.URI url_base { get; construct; }
        }
        
        public class ServiceInfo {
                [CCode (has_construct_function = false)]
                public ServiceInfo ();
                
                public Soup.URI url_base { get; construct; }
        }
        
        public class ServiceActionInfo {
                [CCode (has_construct_function = false)]
                public ServiceActionInfo ();
                
                public weak GLib.List<ServiceActionArgInfo> arguments;
        }
        
        public class ServiceStateVariableInfo {
                [CCode (has_construct_function = false)]
                public ServiceStateVariableInfo ();

                public weak GLib.List<string> allowed_values;
        }

        [CCode (ref_function = "", unref_function = "")]
        public class ServiceAction {
        }
}
